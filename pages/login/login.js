Page({
    data: {
        phone: '',
        phoneErrorMsg: '',
        sms: '',
        smsMsg: ''
    },
    onLoad: function () {
        this.setData({})
    },
    login() {
        if (this.data.phone.length !== 11) {
            this.setData({
                phoneErrorMsg: '手机号输入错误'
            })
        } else if (this.data.sms.length !== 6) {
            this.setData({
                smsMsg: '验证码输入错误'
            })
        } else {
            wx.setStorageSync('login', true);
            wx.redirectTo({
                url: '/pages/index/index'
            })
        }
    },
    inputNumber(e) {
        if (this.data.phoneErrorMsg !== '') {
            this.setData({
                phoneErrorMsg: ''
            })
        }
        this.setData({
            phone: e.detail
        })
    },
    inputSms(e) {
        if (this.data.smsMsg !== '') {
            this.setData({
                smsMsg: ''
            })
        }
        this.setData({
            sms: e.detail
        })
    },
    getuserinfo(e) {
        console.log(e.detail)
        wx.setStorageSync('login', true);
        wx.redirectTo({
            url: '/pages/index/index'
        })
    }
})
