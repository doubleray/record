//获取应用实例
const app = getApp()

Page({
    data: {
        active: 1
    },
    tabbarChange(event) {
        // console.log(event.detail);
        if (event.detail === 0) {
            wx.redirectTo({
                url: '/pages/index/index'
            })
        }
    },
    logout() {
        wx.redirectTo({
            url: '/pages/login/login'
        })
    }
})
