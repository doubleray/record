const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()

    return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
}

//秒转化成 时分秒
function secondToDate(result) {
    var m = Math.floor((result / 60 % 60));
    var s = Math.floor((result % 60));
    return [m, s].map(formatNumber).join(':');
}

module.exports = {
    formatTime: formatTime,
    secondToDate
}
